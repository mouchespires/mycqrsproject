using MediatR;
using MyCqrsProject.Commands;
using MyCqrsProject.Models;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace MyCqrsProject.Handlers
{
    public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, Customer>
    {
        public Task<Customer> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = CreateCustomerCommandHandler.MemoryStore.FirstOrDefault(c => c.Id == request.Id);
            if (customer == null)
            {
                return Task.FromResult<Customer>(null); // Ou lidar com o caso de não encontrado de outra forma
            }

            // Atualiza os dados do cliente
            customer.Name = request.Name;
            customer.Email = request.Email;

            // Em uma aplicação real, aqui você persistiria as mudanças no banco de dados

            return Task.FromResult(customer);
        }
    }
}
