using MediatR;
using MyCqrsProject.Commands;
using MyCqrsProject.Models;

namespace MyCqrsProject.Handlers
{
    public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, Customer>
    {
        // Simulando uma MemoryStore (em um cenário real, você injetaria um serviço de repositório aqui)
        public static List<Customer> MemoryStore = new();

        public Task<Customer> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Email = request.Email
            };

            // Adiciona o cliente à "MemoryStore"
            MemoryStore.Add(customer);

            return Task.FromResult(customer);
        }
    }
}
