using MediatR;
using MyCqrsProject.Models;
using MyCqrsProject.Queries;

namespace MyCqrsProject.Handlers
{
    public class GetAllCustomersQueryHandler : IRequestHandler<GetAllCustomersQuery, List<Customer>>
    {
        public Task<List<Customer>> Handle(GetAllCustomersQuery request, CancellationToken cancellationToken)
        {
            // Retorna todos os clientes da "MemoryStore"
            return Task.FromResult(CreateCustomerCommandHandler.MemoryStore);
        }
    }
}
