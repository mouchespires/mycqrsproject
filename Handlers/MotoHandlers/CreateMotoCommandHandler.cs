using MediatR;
using MyCqrsProject.Commands;
using MyCqrsProject.Models;

namespace MyCqrsProject.Handlers
{
    public class CreateMotoCommandHandler : IRequestHandler<CreateMotoCommand, Moto>
    {
        public static List<Moto> MemoryStore = new();

        public Task<Moto> Handle(CreateMotoCommand request, CancellationToken cancellationToken)
        {
            var moto = new Moto
            {
                Id = Guid.NewGuid(),
                Color = request.Color,
                Branch = request.Branch,
                Model = request.Model,
                Weight = request.Weight
            };

            MemoryStore.Add(moto);


            return Task.FromResult(moto);
        }
    }
}