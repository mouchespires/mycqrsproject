
using MediatR;
using MyCqrsProject.Models;
using MyCqrsProject.Queries;

namespace MyCqrsProject.Handlers
{
    public class GetAllMotoQueryHandler : IRequestHandler<GetAllMotoQuery, List<Moto>>
    {
        public Task<List<Moto>> Handle(GetAllMotoQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(CreateMotoCommandHandler.MemoryStore);
        }
    }
}