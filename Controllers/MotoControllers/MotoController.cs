using MediatR;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using MyCqrsProject.Commands;
using MyCqrsProject.Models;
using MyCqrsProject.Queries;

[Route("api/[controller]")]
[ApiController]
public class MotoController : ControllerBase
{

    private readonly IMediator _mediator;

    public MotoController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<ActionResult<Moto>> CreateMoto(CreateMotoCommand command)
    {
        var moto = await _mediator.Send(command);
        return Ok(moto);
    }

    [HttpGet]
    public async Task<ActionResult<List<Moto>>> GetAllMotos()
    {
        var query = new GetAllMotoQuery();
        var motos = await _mediator.Send(query);

        return Ok(motos);
    }
}