using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyCqrsProject.Commands;
using MyCqrsProject.Models;
using MyCqrsProject.Queries;

[Route("api/[controller]")]
[ApiController]
public class CustomersController : ControllerBase
{
    private readonly IMediator _mediator;

    public CustomersController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<ActionResult<Customer>> CreateCustomer(CreateCustomerCommand command)
    {
        var customer = await _mediator.Send(command);
        return Ok(customer);
    }

    [HttpGet]
    public async Task<ActionResult<List<Customer>>> GetAllCustomers()
    {
        var query = new GetAllCustomersQuery();
        var customers = await _mediator.Send(query);
        return Ok(customers);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<Customer>> UpdateCustomer(Guid id, UpdateCustomerCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest("ID na URL não coincide com o ID no corpo da requisição.");
        }

        var updatedCustomer = await _mediator.Send(command);

        if (updatedCustomer == null)
        {
            return NotFound($"Cliente com ID {id} não encontrado.");
        }

        return Ok(updatedCustomer);
    }

}
