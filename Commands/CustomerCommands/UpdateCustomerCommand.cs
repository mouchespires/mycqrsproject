using MediatR;
using MyCqrsProject.Models;

namespace MyCqrsProject.Commands
{
    public class UpdateCustomerCommand : IRequest<Customer>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
