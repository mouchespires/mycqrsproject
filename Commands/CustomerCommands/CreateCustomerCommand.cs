using MediatR;
using MyCqrsProject.Models;

namespace MyCqrsProject.Commands
{
    public class CreateCustomerCommand : IRequest<Customer>
    {
        public string Name { get; set; }
        public string Email { get; set; }

    }
}