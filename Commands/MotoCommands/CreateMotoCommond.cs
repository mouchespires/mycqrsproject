using MediatR;
using MyCqrsProject.Models;

namespace MyCqrsProject.Commands
{
    public class CreateMotoCommand : IRequest<Moto>
    {
        public string Color { get; set; }
        public string Branch { get; set; }
        public string Model { get; set; }
        public int Weight { get; set; }
    }
}