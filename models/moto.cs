
namespace MyCqrsProject.Models;

public class Moto
{
    public Guid Id { get; set; }
    public string? Color { get; set; }
    public string? Branch { get; set; }
    public string? Model { get; set; }
    public int? Weight { get; set; }
}