using MediatR;
using MyCqrsProject.Models;

namespace MyCqrsProject.Queries
{
    public class GetAllMotoQuery : IRequest<List<Moto>>
    {
    }
}