using MediatR;
using MyCqrsProject.Models;

namespace MyCqrsProject.Queries
{
    public class GetAllCustomersQuery : IRequest<List<Customer>>
    {
    }
}
